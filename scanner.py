import os
import json
import pandas
import MySQLdb
import adlightning as adl
import confiant as conf


con = MySQLdb.connect(read_default_file='/var/auth/.my.conf-crid', db='analytics')
cur = con.cursor()
q = """
SELECT crid, tag, dsp_id
FROM crid_scan_queue
WHERE date BETWEEN DATE_SUB((SELECT MAX(date) FROM crid_scan_queue), INTERVAL 1.5 HOUR) AND (SELECT MAX(date) FROM crid_scan_queue)
	AND tag_sent_to_mediatrust = 1
"""
cur.execute(q)
data = [cur.fetchone()]
while data[len(data)-1] is not None:
	data.append(cur.fetchone())
data = [{'crid': i[0], 'tag': i[1], 'dsp_id': i[2]} for i in data if i is not None]
data = pandas.DataFrame(data)
data = data[['crid', 'tag', 'dsp_id']]
cur.close()
con.close()
print("Fetched Data.  Submitting CRIDs to be scanned")
print("Found {0} CRIDs".format(len(data)))

adl.submit_crids(data)
# conf.submit_tags(data)

