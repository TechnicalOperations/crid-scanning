import json
import pandas
import requests
from copy import deepcopy


pandas.set_option('display.width', 5000)

url = 'http://adtag.prod.adlightning.com/api/1.0/creative/submit'
headers = {
	'Api-key': '58ad4150-5ee2-4a29-874d-3eabc894309a',
	'Content-Type': 'application/json',
}


def mkcreative(x):
	creative = {
		'segment_key': 'site_id',
		'creative_id': x['crid_new_id'],
		'creative_tag': x['tag'],
		'unit_dimensions': '300x250',
		'check_malware': True,
		'check_redirects': True
	}
	print(creative)
	x = y
	return creative

# data columns - crid, tag
def submit_crids(data):
	data['crid_new_id'] = data.apply(lambda x: '{0}_{1}'.format(x['crid'], x['dsp_id']), axis=1)
	data['scrids'] = data.apply(mkcreative, axis=1)
	
	reqs = list(data['scrids'])
	responses = []
	for i in range(0, len(reqs), 100):
		req = {
			'partner_id': 7,
			'creatives': reqs[i:i+100]
		}
		res = requests.post(url, headers=headers, json=req).text
		try:
			responses.append(json.loads(res))
		except:
			pass
	with open('adl_batch.json', 'w') as f:
		f.write(json.dumps(responses))


def fetch_results():
	with open('adl_batch.json', 'r') as f:
		batch_ids = json.loads(f.read())
	responses = []
	for res in batch_ids:
		batch_id = res['batch_id']
		res = requests.post(url.replace('submit', 'get'), headers=headers, json={'partner_id': 7, 'batch_id': batch_id}).text
		try:
			responses.append(json.loads(res))
		except:
			pass
	with open('adlightning_results.json', 'w') as f:
		f.write(json.dumps(responses))

