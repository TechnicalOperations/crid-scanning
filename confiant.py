import requests
from base64 import b64encode as b64e
from time import sleep
import json

BASE= 'https://api.confiant.com/rest/13/tag/verify'
API_KEY = 'bKYJbI7757UqkDWTwjl7Hhz8M5ktaeZD6vC3Ln41MLY'

def tag_verify(row):
	q = {
		'key': API_KEY,
		'tag': b64e(row['tag'].encode('utf-8')).decode('utf-8')
	}
	res = requests.post(BASE, data=q)
	print(res.text)
	try:
		instance = json.loads(res.text)
	except:
		return {'error': 'Error in parsing json', 'data': res.text}
	tag_id = instance['tag_id']
	instance = instance['taginstance']
	q = {
		'key': API_KEY,
		'id': tag_id,
		'taginstance': instance
	}
	return q


def get_tag(q):
	try:
		res = json.loads(requests.post(BASE, data=q).text)
		attempts = 0
		while res['status'] != 'OK' and attempts < 10:
			res = json.loads(requests.post(BASE, data=q).text)
			print(res)
			attempts += 1
			if res['status'] == 'OK':
				break
			sleep(60)
		print(q)
	except:
		res = {}
	return res


def submit_tags(sf_crids):
	sf_crids['results'] = sf_crids.apply(tag_verify, axis=1)
	sf_crids.to_csv('confiant_results.csv', index=False)


def retrieve_tags(sf_crids):
	sf_crids['results'] = sf_crids['results'].apply(get_tag)
	sf_crids.to_csv('confiant_results.csv', index=False)

