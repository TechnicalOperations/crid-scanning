import os
import json
import pandas
import MySQLdb
import adlightning as adl
import confiant as conf

data = pandas.read_csv('confiant_results.csv')
data['scrids'] = data['scrids'].apply(eval)
data['results'] = data['results'].apply(eval)

# conf.retrieve_tags(data)
adl.fetch_results()

with open('adlightning_results.json', 'r') as f:
	adls = json.loads(f.read())
adl_out = []
for adl in adls:
	adl = adl['creatives']
	adl_out += [(i['creative_id'], i['tag'], json.dumps(i)) for i in adl if i['scanned']]
print(len(adl_out))

# conf = pandas.read_csv('confiant_results.csv')
# conf = conf[['crid', 'tag', 'results']].to_dict(orient='records')
# conf = [(i['crid'], i['tag'], json.dumps(i['results']),) for i in conf]
# 
# print("Retrieved results, uploading to database now")
# con = MySQLdb.connect(read_default_file='/var/auth/.my.conf', db='crid_scan')
# cur = con.cursor()
# cur.executemany("INSERT INTO adlightning (crid, tag, response) VALUES (%s, %s, %s)", adl)
# cur.executemany("INSERT INTO confiant (crid, tag, response) VALUES (%s, %s, %s)", conf)
# con.commit()
# con.close()

# os.remove('adlightning_results.json')
# os.remove('adl_batch.json')
# os.remove('confiant_results.csv')
